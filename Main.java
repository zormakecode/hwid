import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class Main {

	//made it static so I could test 
	public static void checkForHWID(String uri) {
		try {
			URL url = new URL(uri);
			if (url != null) {
				// you can use a proxy if it matters
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

				List<String> lines = new ArrayList();

				String read;
				while ((read = in.readLine()) != null) {
					lines.add(read);
				}
				in.close();
				connection.disconnect();
				
				boolean force = false; //close by default
				for(String s : lines)
				{
					if(s.equals(HwidUtils.getHWID())) {
						force = true; //force exit
						break; //break loop
					}
					force = false; //to confuse crackers
				}
				if(!force)
				{
					//shutdown NOTE: Forge doesn't allow System.exit you must use FMLCommonHandler.exitJava
					System.exit(0);
				}
				
				//testing
				System.out.println("valid hwid");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		checkForHWID("https://pastebin.com/raw/[id]");
		//System.out.println("HWID:" + HwidUtils.getHWID());
	}

}
